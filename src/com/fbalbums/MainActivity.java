package com.fbalbums;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.facebook.HttpMethod;
import com.facebook.LoggingBehavior;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionLoginBehavior;
import com.facebook.SessionState;
import com.facebook.Settings;
import com.facebook.Session.OpenRequest;
import com.facebook.model.GraphObject;
import com.fbalbums.adapter.AlbumListAdapter;
import com.fbalbums.model.Album;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;

public class MainActivity extends Activity {

	private ListView listView;
	private Button btnLogin;
	ArrayList<Album> albumList = new ArrayList<Album>();
	private Session.StatusCallback statusCallback = new SessionStatusCallback();
	AlbumListAdapter albumListAdapter;
	ProgressDialog pdDialog;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		pdDialog=new ProgressDialog(this);
		getSHA();

		listView = (ListView) findViewById(R.id.lv_albums);
		btnLogin = (Button) findViewById(R.id.btn_login);

		btnLogin.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				onClickLogin();
			}
		});
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
				
				Intent intent=new Intent(getApplicationContext(),PhotosActivity.class);
				intent.putExtra("id",albumList.get(position).getId());
				intent.putExtra("name",albumList.get(position).getName());
				startActivity(intent);
				
				
			}
		});

		Settings.addLoggingBehavior(LoggingBehavior.INCLUDE_ACCESS_TOKENS);
		Session session = Session.getActiveSession();
		if (session == null) {
			if (savedInstanceState != null) {
				session = Session.restoreSession(this, null, statusCallback, savedInstanceState);
			}
			if (session == null) {
				session = new Session(this);
			}
			Session.setActiveSession(session);
			if (session.getState().equals(SessionState.CREATED_TOKEN_LOADED)) {
				OpenRequest openRequest = new OpenRequest(this);
				openRequest.setLoginBehavior(SessionLoginBehavior.SSO_WITH_FALLBACK);
				openRequest.setPermissions("user_photos");
				session.openForRead(openRequest.setCallback(statusCallback));
			}
			
		}
		
		updateView();
	}

	private class SessionStatusCallback implements Session.StatusCallback {
		@Override
		public void call(Session session, SessionState state, Exception exception) {
			if (session.isOpened()) {

				for (int i = 0; i < session.getPermissions().size(); i++)
					Log.v("permissions", session.getPermissions().get(i));
				updateView();

			}

		}
	}

	private void updateView() {
		Session session = Session.getActiveSession();
		if (session.isOpened()) {
			btnLogin.setVisibility(View.GONE);
			if(albumList.size()==0)
				getAlbums();
		}
		
	}

	private void getAlbums() {

		pdDialog.setMessage("Fetcing albums");
		pdDialog.show();
		Bundle parameters = new Bundle();
		Request request = new Request(Session.getActiveSession(), "me/albums", parameters, HttpMethod.GET,
				new Request.Callback() {

					@Override
					public void onCompleted(Response response) {
						albumList.clear();
						Log.e("message", "albums" + response.toString());
						GraphObject go = response.getGraphObject();
						JSONObject jso = go.getInnerJSONObject();
						JSONArray jarray;
						try {
							jarray = jso.getJSONArray("data");
							for (int i = 0; i < jarray.length(); i++) {

								JSONObject jObject = (JSONObject) jarray.get(i);
								String name = jObject.getString("name");
								
								//int count = jObject.getInt("count");
								long id = jObject.getLong("id");
								albumList.add(new Album(name, id));
								Log.e("albumlst",albumList.size()+"");
							}
						} catch (JSONException e) {
							e.printStackTrace();
						}
						pdDialog.dismiss();
					setAdapter();
					}
				});
		request.executeAsync();

	}

	private void setAdapter() {
		albumListAdapter=new AlbumListAdapter(getApplicationContext(),R.layout.album_list_row,albumList);
		Log.e("albumlst",albumList.size()+"");
		listView.setAdapter(albumListAdapter);

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data);
	}

	private void getSHA() {
		try {
			PackageInfo info = getPackageManager().getPackageInfo("com.fbalbums",
					PackageManager.GET_SIGNATURES);
			for (Signature signature : info.signatures) {
				MessageDigest md = MessageDigest.getInstance("SHA");
				md.update(signature.toByteArray());
				Log.e("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
			}
		} catch (NameNotFoundException e) {
		} catch (NoSuchAlgorithmException e) {
		}

	}

	private void onClickLogin() {
		Session session = Session.getActiveSession();
		if (!session.isOpened() && !session.isClosed()) {
			OpenRequest openRequest = new OpenRequest(this);
			openRequest.setLoginBehavior(SessionLoginBehavior.SSO_WITH_FALLBACK);
			openRequest.setPermissions("user_photos");
			session.openForRead(openRequest.setCallback(statusCallback));
		} else {
			Session.openActiveSession(this, true, statusCallback);
		}
	}

	@Override
	public void onStart() {
		super.onStart();
		Session.getActiveSession().addCallback(statusCallback);
	}

	@Override
	public void onStop() {
		super.onStop();
		Session.getActiveSession().removeCallback(statusCallback);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		Session session = Session.getActiveSession();
		Session.saveSession(session, outState);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
