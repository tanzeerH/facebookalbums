package com.fbalbums.model;

public class Album {
	
	private String name;
	private int count;
	private long id;
	public Album(String mName,int mCount,long mId) {
		this.name=mName;
		this.count=mCount;
		this.id=mId;
	}
	public Album(String mName,long mId) {
		this.name=mName;
		this.id=mId;
	}
	public void setName(String name)
	{
		this.name=name;
	}

	public String getName()
	{
		return this.name;
	}
	public void setCount(int count)
	{
		this.count=count;
	}
	public int getCount()
	{
		return this.count;
	}
	public void setId(long id)
	{
		this.id=id;
	}
	public long getId()
	{
		return this.id;
	}

}
