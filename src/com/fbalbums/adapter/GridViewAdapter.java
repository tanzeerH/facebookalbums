package com.fbalbums.adapter;

import java.util.ArrayList;

import com.fbalbums.R;
import com.fbalbums.lazylist.ImageLoader;



import android.content.Context;

import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;


public class GridViewAdapter extends BaseAdapter {
	private Context mContext;
	ArrayList<String> imgList;
	int width = 0;

	public GridViewAdapter(Context c) {
		mContext = c;
	}

	public GridViewAdapter(Context c, int width, ArrayList<String> list) {
		this.mContext = c;
		this.width = width;
		this.imgList = list;
	}

	public int getCount() {
		return imgList.size();

	}

	public Object getItem(int position) {
		return null;
	}

	public long getItemId(int position) {
		return 0;
	}

	// create a new ImageView for each item referenced by the Adapter
	public View getView(int position, View convertView, ViewGroup parent) {
		ImageView imageView;
		if (convertView == null) { // if it's not recycled, initialize some
									// attributes
			imageView = new ImageView(mContext);
			// imageView.setBackgroundResource(R.drawable.border);
			imageView.setLayoutParams(new GridView.LayoutParams(width, width));
			Log.e("width", "" + width);
			Log.e("size", "" + imgList.size());
						imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
		} else {
			imageView = (ImageView) convertView;
		}
		ImageLoader imageLoader=new ImageLoader(mContext);
		imageLoader.DisplayImage(imgList.get(position), imageView);
		
		return imageView;
	}

}
