package com.fbalbums.adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.fbalbums.R;
import com.fbalbums.model.Album;

public class AlbumListAdapter extends ArrayAdapter<Album> {
	private Context mContext;

	public AlbumListAdapter(Context context, int textViewResourceId, ArrayList<Album> items) {
		super(context, textViewResourceId, items);
		mContext = context;

	}

	private class ViewHolder {

		TextView title;
		
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		Log.e("position", "" + position);
		ViewHolder holder = null;
		LayoutInflater mInflater = (LayoutInflater) mContext.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.album_list_row, null);
			holder = new ViewHolder();
			holder.title = (TextView) convertView.findViewById(R.id.tv_name);
			//holder.number = (TextView) convertView.findViewById(R.id.tv_number);
			convertView.setTag(holder);

		} else
			holder = (ViewHolder) convertView.getTag();

		Album album=getItem(position);
		
		holder.title.setText(album.getName());
		//holder.number.setText(album.getCount()+" photos.");

		return convertView;
	}

}
