package com.fbalbums;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.model.GraphObject;
import com.fbalbums.adapter.GridViewAdapter;
import com.fbalbums.model.Album;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.GridView;

public class PhotosActivity extends Activity{
	ArrayList<String> imgList=new ArrayList<String>();
	private GridView gridView;
	GridViewAdapter gridViewAdapter;
	int width=0;
	ProgressDialog pdDialog;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_photos);
		pdDialog=new ProgressDialog(this);
		Session session=Session.getActiveSession();
		if(session.isOpened())
		{
			Log.e("session","opened");
		}
		getWidth();
		getImages();
		gridView=(GridView)findViewById(R.id.gv_photos);
				
	}
	private void getImages()
	{
		Intent intent=getIntent();
		String name=intent.getStringExtra("name");
		getActionBar().setTitle(name);
		long id=intent.getLongExtra("id",-1);
		if(id!=-1)
		{
			pdDialog.setMessage("Fetching photos...");
			pdDialog.show();
			Bundle parameters = new Bundle();
			Request request = new Request(Session.getActiveSession(), id+"/photos", parameters, HttpMethod.GET,
					new Request.Callback() {

						@Override
						public void onCompleted(Response response) {
							//Log.e("message", "albums" + response.toString());
							GraphObject go = response.getGraphObject();
							JSONObject jso = go.getInnerJSONObject();
							JSONArray jarray;
							try {
								jarray = jso.getJSONArray("data");
								for (int i = 0; i < jarray.length(); i++) {

									JSONObject jObject = (JSONObject) jarray.get(i);
									String src=jObject.getString("source");
									Log.e("src", src);
									imgList.add(src);
									
								}
							} catch (JSONException e) {
								e.printStackTrace();
							}
					pdDialog.dismiss();
						setAdapter();
						}
					});
			request.executeAsync();

		}
	}
	private void setAdapter()
	{
		gridViewAdapter=new GridViewAdapter(this, (width/2-2),imgList);
		gridView.setAdapter(gridViewAdapter);
	}
	private void getWidth()
	{
		DisplayMetrics displayMetrics=getResources().getDisplayMetrics();
		width=displayMetrics.widthPixels;
	}

}
